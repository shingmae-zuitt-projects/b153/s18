let trainer = {
    name: 'Ash Ketchum',
    age: 10,
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bubasaur"],
    friends: {
        hoenn: ['May', 'Max'],
        kanto: ['Brock', 'Misty']
    },
    talk: function(){
        console.log("Result of dot notation:")
        console.log(this.name)
        console.log("Result of talk notation:")
        console.log(this.pokemon[0] + "! I choose you!")
    }
}

