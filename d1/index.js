/*

OBJECTS:
    -An object is a data type that is used to represent the specific details about a single concept
    -Objects are created via truly braces {} and key-value pairs, separated by a colon
    -To the left of the colon is the key, more commonly known as the property 
    -To the right of the colon is the value
    -When using multiple key-value pairs, separate each one with a comma

*/

let person = {
    firstName: "John",
    lastName: "Smith",
    location: {
        city: "Tokyo",
        country: "Japan"
    },

    emails: ['john@mail.com', 'johnsmith@mail.xyz'],
    brushTeeth: function(){
        console.log(this.firstName + " has brushed his teeth")   
    }
}


function sayHello(){
    console.log("Hello")
}



/*
ACCESSING OBJECT PROPERTIES

Use the dot notation to access an object's property's value
*/

console.log(person.lastName)
console.log(person.emails[0])
console.log(person.location.country)

let example ={
    prop: [
        {
            prop2: ["cat", "dog"]
        }
    ]

}

console.log(example.prop[0].prop2[1])

console.clear()

let Arr = []

console.log(typeof Arr)

let car = {
    name: "Nissan",
    manufactureDate: 1999

}

car.name = 'Honda Civic'
car.manufactureDate = 2019

console.log(car)